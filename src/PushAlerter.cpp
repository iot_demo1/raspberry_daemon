#include "PushAlerter.hpp"
#include "ConfigMgr.hpp"


static std::string
_composeGetRegIdUrl()
{
    const ConfigMgr* pConfigMgr = ConfigMgr::_GetInstance();
    std::string url;

    url += pConfigMgr->GetServiceUrl();
    url += pConfigMgr->GetGcmInfoApi();

    return url;
}

static HttpMgr::Header
_composeGcmHeader()
{
	HttpMgr::Header header;
	std::string     buf;

	buf = "Content-Type: application/json";
	header.Append(buf);

	buf = "Authorization: key=AIzaSyDKT5AQCswEZclcsMT2Dkp3siezA-Jq8fo";
	header.Append(buf);

	return header;
}


PushAlerter::PushAlerter()
	: m_pHttpMgr    { HttpMgr::_GetInstance() }
	, m_getRegIdUrl { _composeGetRegIdUrl() }
	, m_gcmSendUrl  { "https://android.googleapis.com/gcm/send" }
	, m_gcmHeader   { _composeGcmHeader() }
	, m_storeId     { ConfigMgr::_GetInstance()->GetStoreId() }
{
}

bool
PushAlerter::SendPushAlert(const std::vector<std::string>& macAddrs) const
{
	if (macAddrs.empty()) {
		RBD_LOG_WARN("MAC address list is empty.");
		return false;
	}

	// get GCM info from server
	std::string response;

	if (!requestGcmInfo(response, macAddrs)) {
		RBD_LOG_WARN("Failed to get GCM information from server.");
		return false;
	}

	// parse GCM info
	Json::Value rootRecv;

	if (!m_jsonReader.parse(response, rootRecv)) {
		RBD_LOG_DEBUG("Json::Reader::parse() failed: %s",
				m_jsonReader.getFormatedErrorMessages().c_str());
		return false;
	}

	Json::Value regIds = rootRecv["registration_ids"];
	Json::Value msg    = rootRecv["msg"];

	if (!regIds.isArray()) {
		RBD_LOG_WARN("Invalid response format: "
				"\'registration_ids\' is not array object.");
		return false;
	}

	// skip sending GCM if none of MAC address is registered
	if (regIds.empty())
		return true;

	// compose HTTP body
	Json::Value rootSend;
	Json::Value data;

	rootSend["registration_ids"] = regIds;

	data["store_id"] = m_storeId;
	data["msg"]      = msg;
	rootSend["data"] = data;

	return sendGcmRequest(rootSend);
}


bool
PushAlerter::requestGcmInfo(std::string& response,
		const std::vector<std::string>& macAddrs) const
{
	std::string url { m_getRegIdUrl + "?store_id=" + m_storeId };

	for (const auto& macAddr: macAddrs)
		url += "&mac_addr=" + macAddr;

	// perform HTTP GET request
	return m_pHttpMgr->Get(url, nullptr, &response);
}

bool
PushAlerter::sendGcmRequest(const Json::Value& root) const
{
	const std::string gcmBody(m_jsonWriter.write(root));

	RBD_LOG_DEBUG("[GCM body]: %s", gcmBody.c_str());

	// perform HTTP POST request
	return m_pHttpMgr->Post(m_gcmSendUrl, &m_gcmHeader, &gcmBody);
}

