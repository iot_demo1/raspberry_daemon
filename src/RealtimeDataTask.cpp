#include "RealtimeDataTask.hpp"
#include "ConfigMgr.hpp"
#include "TimerMgr.hpp"

#include <wiringPi.h>

#define ENABLE_WIRINGPI 0


static std::string
_composeUpdateUrl()
{
    const ConfigMgr* pConfigMgr = ConfigMgr::_GetInstance();
    std::string url;

    url += pConfigMgr->GetServiceUrl();
    url += pConfigMgr->GetRealtimeDataApi();
	url += "?store_id=";
	url += pConfigMgr->GetStoreId();

    return url;
}

static HttpMgr::Header
_composeUpdateHeader()
{
	HttpMgr::Header header;

	header.Append("Content-Type:application/json");

	return header;
}


RealtimeDataTask::RealtimeDataTask()
	: m_pHttpMgr     { HttpMgr::_GetInstance() }
	, m_idTimer      { TimerMgr::_GetInstance()->CreateTimer() }
	, m_bRunning     { false }
	, m_updateUrl    { _composeUpdateUrl() }
	, m_updateHeader { _composeUpdateHeader() }
	, m_storeId      { ConfigMgr::_GetInstance()->GetStoreId() }
{
	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);

#if ENABLE_WIRINGPI
    wiringPiSetup();
#endif
}

RealtimeDataTask::~RealtimeDataTask()
{
	TimerMgr::_GetInstance()->DeleteTimer(m_idTimer);

	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);
}

bool
RealtimeDataTask::Start()
{
	const int period    = ConfigMgr::_GetInstance()->GetRealtimeDataPeriod();
	const auto callback = std::bind(&RealtimeDataTask::updateToServer, this);

	if (!TimerMgr::_GetInstance()->StartTimer(m_idTimer, period, callback)) {
		RBD_LOG_WARN("Failed to start timer.");
		return false;
	}

	// start thread
	m_tMonitor = std::move(std::thread(&RealtimeDataTask::monitorTask, this));

	RBD_LOG_DEBUG("RealtimeDataTask started.");

	return true;
}

bool
RealtimeDataTask::Stop()
{
	if (!TimerMgr::_GetInstance()->StopTimer(m_idTimer)) {
		RBD_LOG_WARN("Failed to stop timer.");
		return false;
	}

	m_bRunning = false;
	m_tMonitor.join();

	RBD_LOG_DEBUG("RealtimeDataTask stopped.");

	return true;
}


void
RealtimeDataTask::monitorTask()
{
	RBD_LOG_INFO("Start collecting realtime data ...");

	m_bRunning = true;

	while (m_bRunning) {
		// keep monitoring sensor status
#if ENABLE_WIRINGPI
        uint8_t luminance = analogRead(0);
#endif

		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

bool
RealtimeDataTask::updateToServer()
{
	RBD_LOG_NOTICE("There is an update of realtime data.");
	RBD_LOG_NOTICE("Requesting to update server ...");

	return true;
#if 0
	// send HTTP POST to server to update realtime data of the store
	// when there is a change of the RealtimeData
	Json::Value root;
	Json::Value data;

	// [TODO] fill the realtime data as JSON format

	const std::string updateBody(m_jsonWriter.write(root));

	// perform HTTP POST request
	return m_pHttpMgr->Post(m_updateUrl, &m_updateHeader, &updateBody);
#endif
}

