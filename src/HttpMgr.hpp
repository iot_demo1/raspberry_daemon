#pragma once

#include "Common.hpp"

#include <curl/curl.h>


// HttpMgr class declaration
class HttpMgr {
	public:
		// HttpMgr::Header class declaration
		class Header {
			public:
				Header();
				Header(Header&& header);	// move ctor
				~Header();

				bool Append(const std::string& str);

				// casting operator to curl_slist*
				explicit operator curl_slist*() const
				{
					return m_pHeaderList;
				}

			private:
				curl_slist* m_pHeaderList;
		}; // class HttpMgr::Header

	public:
		static HttpMgr* _GetInstance();
		static void     _DeleteInstance();

		// HTTP GET
		bool Get(const std::string& url, const Header* pHeader,
				std::string* pResponse = nullptr) const;
		// HTTP POST
		bool Post(const std::string& url, const Header* pHeader,
				const std::string* pBody,
				std::string* pResponse = nullptr) const;

	private:
		HttpMgr();
		~HttpMgr();

	private:
		static HttpMgr* s_pInst;

		CURL* m_phCurl;
}; // class HttpMgr

