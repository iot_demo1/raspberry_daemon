#pragma once

#include <system_error>

#include "Logger.hpp"

#define THROW_SYSTEM_ERROR(desc) \
{ \
	RBD_LOG_ERROR("Exception occurred! (System error)") \
	throw std::system_error(errno, std::system_category(), desc); \
}

#define THROW_RUNTIME_ERROR(desc) \
{ \
	RBD_LOG_ERROR("Exception occurred! (Runtime error)") \
	throw std::runtime_error(desc); \
}

