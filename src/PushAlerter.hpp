#pragma once

#include "Common.hpp"
#include "HttpMgr.hpp"

#include <json/json.h>


// PushAlerter class declaration
class PushAlerter {
	public:
		PushAlerter();

		bool SendPushAlert(const std::vector<std::string>& macAddrs) const;

	private:
		bool requestGcmInfo(std::string& response,
				const std::vector<std::string>& macAddrs) const;
		bool sendGcmRequest(const Json::Value& root) const;

	private:
		HttpMgr* m_pHttpMgr;

		mutable Json::Reader     m_jsonReader;
		mutable Json::FastWriter m_jsonWriter;

		const std::string     m_getRegIdUrl;
		const std::string     m_gcmSendUrl;
		const HttpMgr::Header m_gcmHeader;
		const std::string     m_storeId;
}; // class PushAlerter

