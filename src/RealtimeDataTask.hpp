#pragma once

#include "Common.hpp"
#include "HttpMgr.hpp"
#include "RealtimeData.hpp"

#include <thread>


class RealtimeDataTask {
	public:
		RealtimeDataTask();
		~RealtimeDataTask();

		bool Start();
		bool Stop();

	private:
		void monitorTask();

		bool updateToServer();

	private:
		HttpMgr*  m_pHttpMgr;
		int       m_idTimer;

		std::thread m_tMonitor;
		bool        m_bRunning;

		RealtimeData m_realtimeData;

		const std::string     m_updateUrl;
		const HttpMgr::Header m_updateHeader;
		const std::string     m_storeId;
}; // class RealtimeDataTask

