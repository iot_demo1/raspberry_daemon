#include "Common.hpp"
#include "MainTask.hpp"

#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <libgen.h>
#include <getopt.h>
#include <sys/stat.h>


// global static variables
static char _gszRuntimeDir[PATH_MAX];


static void
_printUsage(const char* pszElfName)
{
	std::cout << "Usage: " << pszElfName << " [OPTION]...\n\n";
	std::cout << " Options are:\n";
	std::cout << "      --no-daemon   Run in normal process mode\n";
	std::cout << "  -v, --verbose     Be verbose.\n";
	std::cout << "  -h, --help        Print this help.\n\n";
	std::cout << "Detailed settings can be configured by modifying "
		"'config.json'.\n";
}

static void
_parseOption(int argc, char* argv[], bool& bDaemon, bool& bVerbose)
{
	int c;
	int isDaemon  = 1;
	int isVerbose = 0;
	struct option long_options[] = {
		{ "no-daemon", no_argument, &isDaemon,   0  },
		{ "verbose",   no_argument, nullptr,    'v' },
		{ "help",      no_argument, nullptr,    'h' },
		{ nullptr,     no_argument, nullptr,     0  }
	};

	while (-1 != (c = getopt_long(argc, argv, "vh",
					long_options, nullptr))) {
		switch (c) {
			case 0:
				// OK
				break;
			case '?':
				// unknown option
				exit(EXIT_FAILURE);
				break;
			case 'v':
				isVerbose = 1;
				break;
			case 'h':
				_printUsage(argv[0]);
				exit(EXIT_SUCCESS);
				break;
			default:
				exit(EXIT_FAILURE);
				break;
		}
	}

	bDaemon  = (isDaemon != 0);
	bVerbose = (isVerbose != 0);
}

static void
_saveRuntimePath(const char* pszExeRelPath)
{
	if (!realpath(pszExeRelPath, _gszRuntimeDir))
		THROW_SYSTEM_ERROR("realpath() failed");

	dirname(_gszRuntimeDir);
}

static void
_setDaemonMode()
{
	pid_t pid = fork();

	if (pid < 0)
		THROW_SYSTEM_ERROR("fork() failed");

	// terminate parent process
	if (pid > 0)
		exit(EXIT_SUCCESS);

	// change the file mode mask
	umask(0);

	// let the child process become session leader
	if (setsid() < 0)
		THROW_SYSTEM_ERROR("setsid() failed");

	// change the current working directory to root(/)
	if (chdir("/") < 0)
		THROW_SYSTEM_ERROR("chdir() failed");

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
}

static void
_handleSignal(int signal)
{
	if ((signal == SIGINT) || (signal == SIGTERM)) {
		RBD_LOG_NOTICE("SIGINT(^C) or SIGTERM received!");
		MainTask::_Quit();
	}
}

static void
_registerSignalHandler()
{
	struct sigaction handler;
	
	handler.sa_handler = _handleSignal;
	handler.sa_flags   = 0;

	if (sigfillset(&handler.sa_mask) < 0)
		THROW_SYSTEM_ERROR("sigfillset() failed");

	if (sigaction(SIGINT, &handler, 0) < 0)
		THROW_SYSTEM_ERROR("sigaction() failed");
	if (sigaction(SIGTERM, &handler, 0) < 0)
		THROW_SYSTEM_ERROR("sigaction() failed");
}


int
main(int argc, char* argv[])
{
	int  status;
	bool bDaemon;
	bool bVerbose;

	// parse options - check daemon mode & verbose
	_parseOption(argc, argv, bDaemon, bVerbose);

	// Initialize logging system
	Logger::_Init(bVerbose, bDaemon);

	// main block
	try {
		_saveRuntimePath(argv[0]);	// save the path of this executable

		if (bDaemon) {
			RBD_LOG_NOTICE("Run as a daemon process!");
			_setDaemonMode();
		}
		else
			RBD_LOG_NOTICE("Run as a normal process!");

		RBD_LOG_DEBUG("Runtime directory = (%s)", _gszRuntimeDir);

		_registerSignalHandler();

		MainTask::_Run();
		status = EXIT_SUCCESS;
	}
	catch (const std::exception& e) {
		RBD_LOG_ERROR(e.what());
		status = EXIT_FAILURE;
	}

	// Finalize logging system
	Logger::_Fini();

	return status;
}


const char*
GetRuntimeDir()
{
	return _gszRuntimeDir;
}

