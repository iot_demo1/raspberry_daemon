#include "MainTask.hpp"
#include "ConfigMgr.hpp"
#include "TimerMgr.hpp"
#include "HttpMgr.hpp"
#include "PushAlerter.hpp"

#include <thread>
#include <unistd.h>


// static class member variable declaration
/*static*/ MainTask* MainTask::s_pInst = nullptr;


/*static*/ void
MainTask::_Run()
{
	createInstance();

	try {
		// run main loop - this will block the main thread
		s_pInst->runLoop();
	}
	catch (const std::exception& e) {
		destroyInstance();
		throw e;
	}

	destroyInstance();
	RBD_LOG_INFO("MainTask stopped.");
}

/*static*/ void
MainTask::_Quit()
{
	if (!s_pInst)
		return;

	// MainTask::quitLoop() should be invoked by other thread
	std::thread t([]() { s_pInst->quitLoop(); });

	t.detach();
}


/*static*/ void
MainTask::createInstance()
{
	if (!s_pInst)
		s_pInst = new MainTask;
}

/*static*/ void
MainTask::destroyInstance()
{
	delete s_pInst;
	s_pInst = nullptr;
}


MainTask::MainTask()
{
	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);

	// store original terminal mode
	tcgetattr(STDIN_FILENO, &m_origt);

	// pre-creation of global singleton objects
	ConfigMgr::_GetInstance();
	TimerMgr::_GetInstance();
	HttpMgr::_GetInstance();

	// start submodules
	if (!m_realtimeDataTask.Start())
		THROW_RUNTIME_ERROR("Error in RealtimeDataTask::Start().");
	if (!m_mobileDetectTask.Start())
		THROW_RUNTIME_ERROR("Error in MobileDetectTask::Start().");
}

MainTask::~MainTask()
{
	// stop submodules
	if (!m_mobileDetectTask.Stop())
		THROW_RUNTIME_ERROR("Error in MobileDetectTask::Stop().");
	if (!m_realtimeDataTask.Stop())
		THROW_RUNTIME_ERROR("Error in RealtimeDataTask::Stop().");

	// delete global singleton objects
	ConfigMgr::_DeleteInstance();
	TimerMgr::_DeleteInstance();
	HttpMgr::_DeleteInstance();

	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);
}

void
MainTask::runLoop()
{
	const std::chrono::milliseconds interval(200);

	m_bLoop = true;

	setTermNonCanonical();
	while (m_bLoop) {
		handleUserCommand();
		std::this_thread::sleep_for(interval);
	}
	setTermCanonical();
}

void
MainTask::quitLoop()
{
	m_bLoop = false;
}

void
MainTask::handleUserCommand() const
{
	timeval tv = { 0, 0 };
	fd_set  rfds;

	FD_ZERO(&rfds);
	FD_SET(STDIN_FILENO, &rfds);

	int retval = select(STDIN_FILENO + 1, &rfds, nullptr, nullptr, &tv);

	if (retval < 0)
		THROW_SYSTEM_ERROR("select() failed");
	if (retval == 0)
		return;

	char buf[8];

	if (0 > (read(STDIN_FILENO, buf, sizeof(buf))))
		THROW_SYSTEM_ERROR("read() failed");

	switch (buf[0]) {
		case 'd': // debug
			debugMode();
			break;
		case 'q': // quit
			_Quit();
			break;
		default:
			// ignore unknown commands
			break;
	}
}

void
MainTask::debugMode() const
{
	RBD_LOG_DEBUG("Entering debug mode.");

	// [TODO] make stdout/stderr silent

	PushAlerter pushAlerter;
	std::string macAddr;

	setTermCanonical();
	std::cout << "Enter MAC address: ";
	std::cin >> macAddr;
	setTermNonCanonical();

	if (pushAlerter.SendPushAlert(std::vector<std::string>(1, macAddr)))
		std::cout << "Sending push alert success." << '\n';
	else
		std::cout << "Sending push alert failed." << '\n';

	RBD_LOG_DEBUG("Exiting debug mode.");
}

void
MainTask::setTermCanonical() const
{
	tcsetattr(STDIN_FILENO, TCSANOW, &m_origt);
}

void
MainTask::setTermNonCanonical() const
{
	termios newt;

	newt = m_origt;
	newt.c_lflag &= ~(ICANON);

	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
}

