#include "HttpMgr.hpp"

#include <sstream>


static size_t
_writeCallback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
	std::stringstream& buf = *(reinterpret_cast<std::stringstream*>(userdata));
	size_t cbData = size * nmemb;

	buf.write(ptr, cbData);

	return cbData;
}

static bool
_performRequest(CURL* phCurl, std::string* pResponse)
{
	CURLcode res;
	std::stringstream buffer;

	// set write callback and data
	curl_easy_setopt(phCurl, CURLOPT_WRITEFUNCTION, _writeCallback);
	curl_easy_setopt(phCurl, CURLOPT_WRITEDATA, &buffer);

	// perform
	res = curl_easy_perform(phCurl);
	if (res != CURLE_OK) {
		RBD_LOG_WARN("curl_easy_perform() failed: %s", curl_easy_strerror(res));
		return false;
	}

	if (pResponse)
		*pResponse = std::move(buffer.str());

	return true;
}


// static class member variable declaration
/*static*/ HttpMgr* HttpMgr::s_pInst = nullptr;


HttpMgr::Header::Header()
	: m_pHeaderList { nullptr }
{
}

HttpMgr::Header::Header(Header&& header)
	: m_pHeaderList { header.m_pHeaderList }
{
	header.m_pHeaderList = nullptr;
}

HttpMgr::Header::~Header()
{
	if (m_pHeaderList) {
		curl_slist_free_all(m_pHeaderList);
		m_pHeaderList = nullptr;
	}
}

bool
HttpMgr::Header::Append(const std::string& str)
{
	curl_slist* pNew = curl_slist_append(m_pHeaderList, str.c_str());

	if (!pNew) {
		RBD_LOG_NOTICE("curl_slist_append() failed.");
		return false;
	}

	m_pHeaderList = pNew;

	return true;
}


/*static*/ HttpMgr*
HttpMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new HttpMgr;

	return s_pInst;
}

/*static*/ void
HttpMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}


bool
HttpMgr::Get(const std::string& url, const HttpMgr::Header* pHeader,
		std::string* pResponse/*=nullptr*/) const
{
	bool  res;
	CURL* phCurl;

	phCurl = curl_easy_init();
	if (!phCurl) {
		RBD_LOG_NOTICE("curl_easy_init() failed.");
		return false;
	}

	RBD_LOG_DEBUG("Requesting URL (%s) ...", url.c_str());

	// set HTTP GET options
	curl_easy_setopt(phCurl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(phCurl, CURLOPT_HTTPGET, 1L);
	if (pHeader) {
		curl_easy_setopt(phCurl, CURLOPT_HTTPHEADER,
				static_cast<curl_slist*>(*pHeader));
	}

	// perform the request
	res = _performRequest(phCurl, pResponse);
	curl_easy_cleanup(phCurl);

	return res;
}

bool
HttpMgr::Post(const std::string& url, const HttpMgr::Header* pHeader,
		const std::string* pBody, std::string* pResponse/*=nullptr*/) const
{
	bool  res;
	CURL* phCurl;

	phCurl = curl_easy_init();
	if (!phCurl) {
		RBD_LOG_NOTICE("curl_easy_init() failed.");
		return false;
	}

	RBD_LOG_DEBUG("Requesting URL (%s) ...", url.c_str());

	// set HTTP POST options
	curl_easy_setopt(phCurl, CURLOPT_URL, url.c_str());
	if (pHeader) {
		curl_easy_setopt(phCurl, CURLOPT_HTTPHEADER,
				static_cast<curl_slist*>(*pHeader));
	}
	if (pBody)
		curl_easy_setopt(phCurl, CURLOPT_POSTFIELDS, pBody->c_str());

	// perform the request
	res = _performRequest(phCurl, pResponse);
	curl_easy_cleanup(phCurl);

	return res;
}


HttpMgr::HttpMgr()
{
	curl_global_init(CURL_GLOBAL_ALL);

	RBD_LOG_INFO("HttpMgr instance created.");
}

HttpMgr::~HttpMgr()
{
	curl_global_cleanup();
}

