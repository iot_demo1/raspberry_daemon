#pragma once

#include "Common.hpp"
#include "RealtimeDataTask.hpp"
#include "MobileDetectTask.hpp"

#include <termios.h>


// MainTask class declaration
class MainTask {
	public:
		static void _Run();
		static void _Quit();

	private:
		static void createInstance();
		static void destroyInstance();

		MainTask();
		~MainTask();

		void runLoop();
		void quitLoop();
		void handleUserCommand() const;

		void debugMode() const;

		void setTermCanonical() const;
		void setTermNonCanonical() const;

	private:
		static MainTask* s_pInst;

		RealtimeDataTask m_realtimeDataTask;
		MobileDetectTask m_mobileDetectTask;

		termios m_origt;
		bool    m_bLoop;
}; // class MainTask

