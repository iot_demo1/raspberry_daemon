#include "TimerMgr.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/timerfd.h>

#define TIMER_CTRL_FIFO_PATH	"/tmp/.timer_ctrl_fifo"


// static class member variable declaration
/*static*/ TimerMgr* TimerMgr::s_pInst = nullptr;


static void
_armTimer(int fd, int secInterval)
{
	itimerspec its;

	its.it_interval.tv_sec  = secInterval;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_sec     = secInterval;
	its.it_value.tv_nsec    = 0;

	if (timerfd_settime(fd, 0/*relative time*/, &its, nullptr) < 0)
		THROW_SYSTEM_ERROR("timerfd_settime() failed");
}

static void
_disarmTimer(int fd)
{
	return _armTimer(fd, 0);
}

static void
_setFile(int fd)
{
	uint64_t buf = 0;

	if (write(fd, &buf, sizeof(buf)) < 0)
		THROW_SYSTEM_ERROR("write() failed");
}

static void
_clearFile(int fd)
{
	uint64_t buf;

	if (read(fd, &buf, sizeof(buf)) < 0)
		THROW_SYSTEM_ERROR("read() failed");
}


/*static*/ TimerMgr*
TimerMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new TimerMgr;

	return s_pInst;
}

/*static*/ void
TimerMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}


int
TimerMgr::CreateTimer()
{
	int id = timerfd_create(CLOCK_MONOTONIC, 0);

	if (id < 0)
		THROW_SYSTEM_ERROR("timerfd_create() failed");

	return id;
}

void
TimerMgr::DeleteTimer(int& id)
{
	if (id < 0) {
		RBD_LOG_NOTICE("Invalid Timer. (ID=%d)", id);
		return;
	}

	if (IsTimerRunning(id))
		StopTimer(id);

	close(id);
	id = -1;
}

bool
TimerMgr::StartTimer(int id,
		int secInterval, const Callback& callback)
{
	if (id < 0) {
		RBD_LOG_NOTICE("Invalid Timer. (ID=%d)", id);
		return false;
	}
	if (IsTimerRunning(id)) {
		RBD_LOG_NOTICE("Timer is already running. (ID=%d)", id);
		return false;
	}

	std::lock_guard<std::mutex> lock(m_mtxTimer);

	pauseMonitoring();
	m_fdTimerTbl[id] = callback; // append timer descriptor to the table
	resumeMonitoring();

	_armTimer(id, secInterval);

	return true;
}

bool
TimerMgr::StopTimer(int id)
{
	if (id < 0) {
		RBD_LOG_NOTICE("Invalid Timer. (ID=%d)", id);
		return false;
	}
	if (!IsTimerRunning(id)) {
		RBD_LOG_NOTICE("Timer is not running. (ID=%d)", id);
		return false;
	}

	std::lock_guard<std::mutex> lock(m_mtxTimer);

	pauseMonitoring();
	m_fdTimerTbl.erase(id); // remove timer descriptor from the table
	resumeMonitoring();

	_disarmTimer(id);

	return true;
}

bool
TimerMgr::IsTimerRunning(int id) const
{
	std::lock_guard<std::mutex> lock(m_mtxTimer);

	return (m_fdTimerTbl.find(id) != m_fdTimerTbl.end());
}


TimerMgr::TimerMgr()
{
	// create FIFO to send interrupt to blocking select()
	if (mkfifo(TIMER_CTRL_FIFO_PATH, S_IRUSR | S_IWUSR) < 0) {
		if (errno != EEXIST)
			THROW_SYSTEM_ERROR("mkfifo() failed");
	}

	m_fdCtrl = open(TIMER_CTRL_FIFO_PATH, O_RDWR);
	if (m_fdCtrl < 0)
		THROW_SYSTEM_ERROR("open() failed");

	// start thread
	m_tMonitor = std::move(std::thread(&TimerMgr::monitorTask, this));

	RBD_LOG_INFO("TimerMgr instance created.");
}

TimerMgr::~TimerMgr()
{
	// exit thread loop and join
	quitMonitoring();
	m_tMonitor.join();

	// close FIFO
	close(m_fdCtrl);
	m_fdCtrl = -1;

	// close and delete all remain timers
	for (auto e: m_fdTimerTbl)
		close(e.first);

	m_fdTimerTbl.clear();
}

void
TimerMgr::monitorTask()
{
	fd_set fdSet;
	int fdMax;

	m_fQuit = false;

	for (; ; ) {
		// calculate maximum file descriptor
		fdMax = m_fdCtrl;
		if (!m_fdTimerTbl.empty())
			fdMax = std::max(fdMax, m_fdTimerTbl.rbegin()->first);

		// reset descriptor vector
		FD_ZERO(&fdSet);
		FD_SET(m_fdCtrl, &fdSet);
		for (auto e: m_fdTimerTbl)
			FD_SET(e.first, &fdSet);

		// suspend task until descriptor is ready
		if (select(fdMax + 1, &fdSet, nullptr, nullptr, nullptr) <= 0)
			THROW_SYSTEM_ERROR("select() failed");

		if (FD_ISSET(m_fdCtrl, &fdSet)) {
			std::unique_lock<std::mutex> lock(m_mtxWait);

			_clearFile(m_fdCtrl);
			m_cv.notify_one();	// unblock pauseMonitoring()

			if (m_fQuit)
				break;

			m_cv.wait(lock);	// wait for resumeMonitoring()
		}
		else {
			for (auto e: m_fdTimerTbl) {
				if (FD_ISSET(e.first, &fdSet)) {
					_clearFile(e.first);
					e.second();	// invoke callback
				}
			}
		}
	}
}

void
TimerMgr::pauseMonitoring()
{
	std::unique_lock<std::mutex> lock(m_mtxWait);

	_setFile(m_fdCtrl);
	m_cv.wait(lock);
}

void
TimerMgr::resumeMonitoring()
{
	m_cv.notify_one();
}

void
TimerMgr::quitMonitoring()
{
	m_fQuit = true;
	pauseMonitoring();
}

