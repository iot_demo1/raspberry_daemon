#include "ConfigMgr.hpp"

#include <fstream>
#include <sstream>
#include <json/json.h>

#define CONFIG_FILE_NAME	"config.json"


// extern function declaration
extern const char* GetRuntimeDir();


// static class member variable declaration
/*static*/ ConfigMgr* ConfigMgr::s_pInst = nullptr;


/*static*/ ConfigMgr*
ConfigMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new ConfigMgr(GetRuntimeDir());

	return s_pInst;
}

/*static*/ void
ConfigMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}


ConfigMgr::ConfigMgr(const std::string& runDir)
{
	std::ifstream     file(runDir + '/' + CONFIG_FILE_NAME);
	std::stringstream buffer;

	if (!file.is_open())
		THROW_RUNTIME_ERROR("Could not open config file.");

	buffer << file.rdbuf();
	file.close();

	if (!parse(buffer.str()))
		THROW_RUNTIME_ERROR("Failed to parse config.json file.");

	RBD_LOG_INFO("ConfigMgr instance created.");
}

bool
ConfigMgr::parse(const std::string& content)
{
	Json::Reader reader;
	Json::Value  root;
	Json::Value  sub;

	if (!reader.parse(content, root)) {
		RBD_LOG_WARN("Json::Reader::parse() failed: %s",
				reader.getFormatedErrorMessages().c_str());
		return false;
	}

	// general
	m_storeId    = root["store_id"].asString();
	m_serviceUrl = root["service_url"].asString();

	// realtime data
	sub = root["realtime_data"];
	m_realtimeDataApi    = sub["api"].asString();
	m_realtimeDataPeriod = sub["check_period"].asInt();

	// GCM
	sub = root["gcm"];
	m_gcmInfoApi      = sub["api"].asString();
	m_gcmSendPeriod   = sub["send_period"].asInt();
	m_gcmResumePeriod = sub["resume_period"].asInt();

	return true;
}

