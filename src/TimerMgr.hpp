#pragma once

#include "Common.hpp"

#include <functional>
#include <thread>
#include <map>
#include <mutex>
#include <condition_variable>


// TimerMgr class declaration
class TimerMgr {
	typedef std::function<void()> Callback;

	public:
		static TimerMgr* _GetInstance();
		static void      _DeleteInstance();

		int  CreateTimer();
		void DeleteTimer(int& id);

		bool StartTimer(int id,
				int secInterval, const Callback& callback);
		bool StopTimer(int id);
		bool IsTimerRunning(int id) const;

	private:
		TimerMgr();
		~TimerMgr();

		void monitorTask();

		void pauseMonitoring();
		void resumeMonitoring();
		void quitMonitoring();

	private:
		static TimerMgr* s_pInst;

		std::thread m_tMonitor;
		bool        m_fQuit;
		int         m_fdCtrl;
		std::map<int, Callback> m_fdTimerTbl;

		mutable std::mutex      m_mtxTimer;
		std::mutex              m_mtxWait;
		std::condition_variable m_cv;
};

