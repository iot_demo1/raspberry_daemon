#pragma once

#include "Common.hpp"

#include <string>


class ConfigMgr {
	public:
		static ConfigMgr* _GetInstance();
		static void       _DeleteInstance();

		// hard coded store ID
		const std::string& GetStoreId() const
		{
			return m_storeId;
		}

		// service root URL
		const std::string& GetServiceUrl() const
		{
			return m_serviceUrl;
		}

		// API for updating realtime data
		const std::string& GetRealtimeDataApi() const
		{
			return m_realtimeDataApi;
		}

		// period (in second) for updating realtime data
		int GetRealtimeDataPeriod() const
		{
			return m_realtimeDataPeriod;
		}

		// API for querying device's registration ID and so on
		const std::string& GetGcmInfoApi() const
		{
			return m_gcmInfoApi;
		}

		// period (in second) for sending GCM
		int GetGcmSendPeriod() const
		{
			return m_gcmSendPeriod;
		}

		// period (in second) for resuming GCM
		int GetGcmResumePeriod() const
		{
			return m_gcmResumePeriod;
		}

	private:
		ConfigMgr(const std::string& runDir);

		bool parse(const std::string& content);

	private:
		static ConfigMgr* s_pInst;

		std::string m_storeId;
		std::string m_serviceUrl;

		std::string m_realtimeDataApi;
		int         m_realtimeDataPeriod;

		std::string m_gcmInfoApi;
		int         m_gcmSendPeriod;
		int         m_gcmResumePeriod;
}; // ConfigMgr

