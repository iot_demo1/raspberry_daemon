#include "MobileDetectTask.hpp"
#include "ConfigMgr.hpp"
#include "TimerMgr.hpp"


MobileDetectTask::MobileDetectTask()
	: m_pTimerMgr    { TimerMgr::_GetInstance() }
	, m_idTimer      { m_pTimerMgr->CreateTimer() }
	, m_bRunning     { false }
	, m_resumePeriod { ConfigMgr::_GetInstance()->GetGcmResumePeriod() }
{
	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);
}

MobileDetectTask::~MobileDetectTask()
{
	clearDeviceList();
	m_pTimerMgr->DeleteTimer(m_idTimer);

	RBD_LOG_DEBUG(__PRETTY_FUNCTION__);
}

bool
MobileDetectTask::Start()
{
	const int period    = ConfigMgr::_GetInstance()->GetGcmSendPeriod();
	const auto callback = std::bind(&MobileDetectTask::sendGCM, this);

	if (!m_pTimerMgr->StartTimer(m_idTimer, period, callback)) {
		RBD_LOG_WARN("Failed to start timer.");
		return false;
	}

	// start thread
	m_tMonitor = std::move(std::thread(&MobileDetectTask::monitorTask, this));

	RBD_LOG_DEBUG("MobileDetectTask started.");

	return true;
}

bool
MobileDetectTask::Stop()
{
	if (!m_pTimerMgr->StopTimer(m_idTimer)) {
		RBD_LOG_WARN("Failed to stop timer.");
		return false;
	}

	m_bRunning = false;
	m_tMonitor.join();

	RBD_LOG_DEBUG("MobileDetectTask stopped.");

	return true;
}


void
MobileDetectTask::monitorTask()
{
	RBD_LOG_INFO("Start detecting mobile device approached ...");

	m_bRunning = true;

	while (m_bRunning) {
		std::string macAddr;

		// [TODO] keep monitoring approaching mobile devices
		// skip if specified MAC address had been already registered to the list

		if (!macAddr.empty()) {
			std::lock_guard<std::mutex> lock(m_mtx);

			if (m_macProcessed.find(macAddr) == m_macProcessed.end())
				m_macDetected.push_back(macAddr);
			else {
				RBD_LOG_INFO("Skip sending push alert. (MAC address=%s)",
						macAddr.c_str());
			}
		}

		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

bool
MobileDetectTask::sendGCM()
{
	if (m_macDetected.empty()) {
		RBD_LOG_DEBUG("There is no detected mobile devices.");
		return true;
	}

	RBD_LOG_NOTICE("There is one or more mobile devices detected.");
	RBD_LOG_NOTICE("Requesting to send GCM ...");

	bool bRes = m_pushAlerter.SendPushAlert(m_macDetected);

	if (bRes) {
		std::lock_guard<std::mutex> lock(m_mtx);

		// register processed MAC addresses to the list
		if (!registerDeviceToTheList(m_macDetected)) {
			RBD_LOG_NOTICE("Failed to register MAC address to the list.");
			return false;
		}

		m_macDetected.clear();
	}

	return bRes;
}

bool
MobileDetectTask::registerDeviceToTheList(const std::vector<std::string>& macAddrs)
{
	const int idTimer   = m_pTimerMgr->CreateTimer();
	const auto callback = std::bind(&MobileDetectTask::timerExpired, this, idTimer);

	// start timer
	if (!m_pTimerMgr->StartTimer(idTimer, m_resumePeriod, callback)) {
		RBD_LOG_WARN("Failed to start timer for newly added devices.");
		return false;
	}

	std::lock_guard<std::mutex> lock(m_mtx);

	// register to the processed list
	std::copy(macAddrs.begin(), macAddrs.end(),
			std::inserter(m_macProcessed, m_macProcessed.end()));

	// register to the timer table
	m_timerTbl[idTimer] = macAddrs;

	return true;
}

void
MobileDetectTask::clearDeviceList()
{
	m_macProcessed.clear();

	// stop and delete timer of all remain devices
	for (auto e: m_timerTbl) {
		int idTimer = e.first;

		m_pTimerMgr->StopTimer(idTimer);
		m_pTimerMgr->DeleteTimer(idTimer);
	}

	m_timerTbl.clear();
}

void
MobileDetectTask::timerExpired(int idTimer)
{
	m_mtx.lock();

	// remove from the processed list
	for (const auto& macAddr: m_timerTbl.at(idTimer))
		m_macProcessed.erase(macAddr);

	// remove from the timer table
	m_timerTbl.erase(idTimer);

	m_mtx.unlock();

	// stop and delete timer registered
	m_pTimerMgr->StopTimer(idTimer);
	m_pTimerMgr->DeleteTimer(idTimer);
}

