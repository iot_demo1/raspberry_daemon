#pragma once

#include "Common.hpp"
#include "PushAlerter.hpp"

#include <thread>
#include <mutex>
#include <unordered_set>


// forward declaration
class TimerMgr;


class MobileDetectTask {
	public:
		MobileDetectTask();
		~MobileDetectTask();

		bool Start();
		bool Stop();

	private:
		void monitorTask();

		bool sendGCM();

		bool registerDeviceToTheList(const std::vector<std::string>& macAddrs);
		void clearDeviceList();

		void timerExpired(int idTimer);

	private:
		TimerMgr*   m_pTimerMgr;
		int         m_idTimer;

		std::thread m_tMonitor;
		bool        m_bRunning;

		PushAlerter m_pushAlerter;
		std::mutex  m_mtx;

		std::vector<std::string> m_macDetected;
		std::unordered_set<std::string> m_macProcessed;

		std::map<int, std::vector<std::string>> m_timerTbl;
		const int   m_resumePeriod;
}; // class MobileDetectTask

