all:
	@make -C src || exit $?;
	@echo "Build finished."

clean:
	@make -C src clean
