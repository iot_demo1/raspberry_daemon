# README #

## 1. Dependencies (Ubuntu의 apt-get package명 기준) ##

* **g++** : GNU C++ 컴파일러
* **libcurl-gnutls-dev** : cURL 개발 라이브러리 (GnuTLS 기반)
* **libjsoncpp-dev** : C++용 JSON 읽기/쓰기 라이브러리


## 2. How to build ##

단지 make를 실행하면 된다. 성공하면 top 디렉토리에 **raspberryd** 실행파일이 생성된다.


## 3. 실행 옵션 ##

*     --no-daemon : 강제로 일반 모드로 실행한다.
* -v, --verbose   : 디버깅 로그를 출력한다.
* -h, --help      : 실행 도움말을 보여준다.


## 4. 런타임 커맨드 ##

일반 모드(--no-daemon 옵션)로 실행한 경우 키보드를 통해 몇 가지 제어를 할 수 있다.

* 'd' 키 : debug. MAC 주소를 입력하여 GCM 테스트를 수행할 수 있다.
* 'q' 키 : quit. 실행을 종료한다.